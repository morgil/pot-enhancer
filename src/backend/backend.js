document.addEventListener('DOMContentLoaded', function () {
    // Load current settings
    var settingsPromise = Settings.getInstance().load();
    settingsPromise.then(() => {
        // General functions
        let functionsContainer = document.querySelector('#functions');
        let functionsView = new FunctionsView(functionsContainer);

        // Manage emotes
        let emotesContainer = document.querySelector('#emotes');
        let emotesView = new EmotesView(emotesContainer);
        let emotesController = new EmotesController(emotesView);

        // Manage blocked users
        let blockUsersContainer = document.querySelector('#blockUsers');
        let blockUsersView = new BlockUsersView(blockUsersContainer);
        let blockUsersController = new BlockUsersController(blockUsersView);

        // Manage saved posts
        let savePostsContainer = document.querySelector('#savePosts');
        let savePostsView = new SavePostsView(savePostsContainer);
        let savePostsController = new SavePostsController(savePostsView);

        // Manage injecting CSS code
        let injectCssContainer = document.querySelector('#injectCss');
        let injectCssView = new InjectCssView(injectCssContainer);
        let injectCssController = new InjectCssController(injectCssView);

        // Export settings
        let exportSettingsContainer = document.querySelector('#exportSettings');
        let exportSettingsView = new ExportSettingsView(exportSettingsContainer);

        // Import settings
        let importSettingsContainer = document.querySelector('#importSettings');
        let importSettingsView = new ImportSettingsView(importSettingsContainer);

        // Reset settings
        let resetSettingsContainer = document.querySelector('#resetSettings');
        let resetSettingsView = new ResetSettingsView(resetSettingsContainer);
    });
});
