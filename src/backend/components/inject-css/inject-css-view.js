/**
 * View for injecting CSS code.
 *
 * @author FiveFuenf
 */
class InjectCssView extends AbstractView {
    constructor(container) {
        super(container);

        // Define possible events for observers
        this.observers = {
            'save': []
        };

        // Textarea for editing the injected CSS code
        this.codeTextarea = container.querySelector('#injectCssCode');

        // Button to save the injeceted CSS code
        this.saveButton = container.querySelector('#saveInjectCssCode');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        // Capture tab-keys and insert indenting-spaces instead
        this.codeTextarea.addEventListener('keydown', e => {
            if (e.key === 'Tab') {
                e.preventDefault();

                // Insert spaces at caret position
                TextareaUtility.insertAtCaret(this.codeTextarea, '    ')
            }
        });

        // Get current CSS code to inject and set in the textarea
        if (Settings.getInstance().get('injectCssCode')) {
            this.codeTextarea.value = Settings.getInstance().get('injectCssCode');
        }

        // Save injected CSS code on button click
        this.saveButton.addEventListener('click', () => {
            var code = this.codeTextarea.value;
            if (code === undefined || !code) {
                code = '';
            }

            this.trigger('save', {
                'code': code
            });
        });
    }
}
