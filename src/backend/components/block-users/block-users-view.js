/**
 * View for blocking users.
 *
 * @author FiveFuenf
 */
class BlockUsersView extends AbstractView {
    constructor(container) {
        super(container);

        // Define possible events for observers
        this.observers = {
            'save': [],
            'remove': []
        };

        // Button for creating a new blocked user
        this.createBlockUserButton = container.querySelector('#createBlockUser');

        // List all blocked users
        this.blockUserList = container.querySelector('#blockUserList');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        this.createBlockUserButton.addEventListener('click', () => {
            let blockUserRow = this.creatBlockUserRow(true);
            this.blockUserList.prepend(blockUserRow);
        });

        let blockUsers = Settings.getInstance().get('blockUsers');
        Object.keys(blockUsers).forEach((name) => {
            let blockUserRow = this.creatBlockUserRow(false, blockUsers[name]);
            this.blockUserList.prepend(blockUserRow);
        });
    }


    /**
     * Creates a new row defining a blocked user.
     *
     * @param {boolean} isNew Whether the form is created new and empty
     * @param {BlockUser} [blockUser] Existing blocked user to create row for
     *
     * @returns {Element}
     */
    creatBlockUserRow(isNew, blockUser) {
        let inputGroup = document.createElement('div');
        inputGroup.classList.add('input-group');

        // Controls
        let saveButton = document.createElement('button');
        saveButton.classList.add('btn', 'btn-primary');
        saveButton.innerHTML = 'Save';
        saveButton.setAttribute('type', 'button');

        let removeButton = document.createElement('button');
        removeButton.classList.add('btn', 'btn-danger');
        removeButton.innerHTML = 'Remove';
        removeButton.setAttribute('type', 'button');

        // Username to block
        let nameInput = document.createElement('input');
        nameInput.classList.add('form-control');
        nameInput.setAttribute('type', 'text');
        nameInput.setAttribute('placeholder', 'Name of user to block');

        inputGroup.append(saveButton, nameInput, removeButton);

        // Set existing blocked user values
        let previousName;
        if (blockUser !== undefined) {
            previousName = blockUser.name;
            nameInput.value = blockUser.name;
        }

        // Setup events
        saveButton.addEventListener('click', () => {
            let name = nameInput.value;
            if (!name) {
                this.container.append(GeneralUtility.createAlert('warning', 'Fill out name of user to block.', 5000));
                return;
            }

            try {
                this.trigger('save', {
                    'name': name,
                    'isNew': isNew,
                    'previousName': previousName
                });
            } catch (e) {
                this.container.append(GeneralUtility.createAlert('warning', 'There already exists a blocked user with this name.', 5000));
                return;
            }

            isNew = false;
            previousName = name;
        });

        removeButton.addEventListener('click', () => {
            let name = nameInput.value;
            if (!name) {
                inputGroup.remove();
                return;
            }

            this.trigger('remove', name);
            inputGroup.remove();
        });

        return inputGroup;
    }
}
