/**
 * Base class of all post/thread/... text editors.
 *
 * @author FiveFuenf
 */
class AbstractEditor extends AbstractView {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this._container.classList.add('editor');

        // Important DOM objects - will be declared in render()-method
        this._title = null;
        this._message = null;
        this._icon = null;
        this._enableConvertUrls = null;
        this._disableBbcode = null;
        this._disableSmilies = null;
        this._disableEnhancement = null;
        this._submitButton = null;
        this._previewButton = null;
        this._previewContainer = null;
        this._logContainer = null;
    }


    /**
     * @override
     */
    render() {
        super.render();

        let template = `
<div>
    <div class="container bg-gray-400 p-3 mt-5 mb-5">
        <div class="form-group row mb-2">
            <label for="title" class="col-md-2 col-form-label text-left">Titel</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="title" name="title" tabindex="1" />
            </div>
        </div>
        <div class="form-group row">
            <label for="message" class="col-md-2 col-form-label text-left">
                Text
                <div class="remember-post-message">
                    <small class="saved">Saved</small>
                    <small class="restore">Restore</small>
                </div>
            </label>
            <div class="col-md-10 text-left">
                <div class="functions">
                    <button type="button" class="btn btn-dark wrap-bold" title="Fett" tabindex="-1">F</button>
                    <button type="button" class="btn btn-dark wrap-underline" title="Unterstrichen" tabindex="-1">U</button>
                    <button type="button" class="btn btn-dark wrap-italic" title="Kursiv" tabindex="-1">K</button>
                    <button type="button" class="btn btn-dark wrap-strike" title="Durchgestrichen" tabindex="-1">S</button>
                    <button type="button" class="btn btn-dark wrap-monospace" title="Monospace" tabindex="-1">M</button>

                    <div class="smilies dropdown">
                        <button
                            id="smiliesDropdownButton"
                            type="button"
                            class="btn btn-dark dropdown-toggle"
                            data-bs-toggle="dropdown"
                            tabindex="-1"
                        >Smiley</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon3.gif" /></span>
                                <span class="shortcut">8|</span>
                                <span class="description">Unglaeubig gucken</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon12.gif" /></span>
                                <span class="shortcut">:(</span>
                                <span class="description">Traurig</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon7.gif" /></span>
                                <span class="shortcut">:)</span>
                                <span class="description">Lachend</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon4.gif" /></span>
                                <span class="shortcut">:0:</span>
                                <span class="description">Verschmitzt lachen</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/banghead.gif" /></span>
                                <span class="shortcut">:bang:</span>
                                <span class="description">Kopf gegen Wand</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/confused.gif" /></span>
                                <span class="shortcut">:confused:</span>
                                <span class="description">Verwirrt</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/biggrin.gif" /></span>
                                <span class="shortcut">:D</span>
                                <span class="description">Breites Grinsen</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/icon15.gif" /></span>
                                <span class="shortcut">:eek:</span>
                                <span class="description">Erschrocken</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/hm.gif" /></span>
                                <span class="shortcut">:hm:</span>
                                <span class="description">Hmm</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/freaked.gif" /></span>
                                <span class="shortcut">:huch:</span>
                                <span class="description">Haare zu Berge</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon13.gif" /></span>
                                <span class="shortcut">:mad:</span>
                                <span class="description">Wütend</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/mata.gif" /></span>
                                <span class="shortcut">:mata:</span>
                                <span class="description">Mata</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/smiley-pillepalle.gif" /></span>
                                <span class="shortcut">:moo:</span>
                                <span class="description">Pillepalle</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/icon16.gif" /></span>
                                <span class="shortcut">:o</span>
                                <span class="description">Peinlich/erstaunt</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon2.gif" /></span>
                                <span class="shortcut">:p</span>
                                <span class="description">Zunge</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/icon18.gif" /></span>
                                <span class="shortcut">:roll:</span>
                                <span class="description">Augen rollen</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/ugly.gif" /></span>
                                <span class="shortcut">:ugly:</span>
                                <span class="description">Hässlon</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/sceptic.gif" /></span>
                                <span class="shortcut">:what:</span>
                                <span class="description">Skeptisch</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/urgs.gif" /></span>
                                <span class="shortcut">:wurgs:</span>
                                <span class="description">Wurgs</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon11.gif" /></span>
                                <span class="shortcut">:xx:</span>
                                <span class="description">Etwas schlecht finden</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon1.gif" /></span>
                                <span class="shortcut">:zyklop:</span>
                                <span class="description">Zyklop</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/sleepy.gif" /></span>
                                <span class="shortcut">:zzz:</span>
                                <span class="description">Zzz</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/icons/icon8.gif" /></span>
                                <span class="shortcut">:|</span>
                                <span class="description">Meh</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/wink.gif" /></span>
                                <span class="shortcut">;)</span>
                                <span class="description">Zwinkern</span>
                            </a>
                            <a class="dropdown-item smiley">
                                <span class="image"><img src="./img/smilies/icon5.gif" /></span>
                                <span class="shortcut">^^</span>
                                <span class="description">Fröhlich</span>
                            </a>
                        </div>
                    </div>

                    <div class="emotes dropdown d-none">
                        <button
                            id="emotesDropdownButton"
                            type="button"
                            class="btn btn-dark dropdown-toggle"
                            data-bs-toggle="dropdown"
                            tabindex="-1"
                        >Emote</button>
                        <div class="dropdown-menu">
                        </div>
                    </div>

                    <button type="button" class="btn btn-dark insert-url" title="Link" tabindex="-1">Link</button>
                    <button type="button" class="btn btn-dark insert-url-text" title="Textlink" tabindex="-1">Textlink</button>
                    <button type="button" class="btn btn-dark insert-url-image" title="Bildlink" tabindex="-1">Bildlink</button>
                    <button type="button" class="btn btn-dark insert-img" title="Bild" tabindex="-1">Bild</button>
                    <button type="button" class="btn btn-dark insert-video" title="Video" tabindex="-1">Video</button>
                    <button type="button" class="btn btn-dark insert-quote" title="Quote" tabindex="-1">Quote</button>
                    <button type="button" class="btn btn-dark insert-code" title="Code" tabindex="-1">Code</button>
                    <button type="button" class="btn btn-dark insert-tex" title="TeX" tabindex="-1">TeX</button>
                    <button type="button" class="btn btn-dark insert-list" title="Liste" tabindex="-1">Liste</button>
                    <button type="button" class="btn btn-dark insert-spoiler" title="Spoiler" tabindex="-1">Spoiler</button>
                    <button type="button" class="btn btn-dark insert-trigger" title="Trigger" tabindex="-1">Trigger</button>
                    <button type="button" class="btn btn-dark insert-table" title="Tabelle" tabindex="-1">Tabelle</button>
                </div>

                <textarea class="form-control" id="message" name="message" tabindex="4"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="icon" class="col-md-2 col-form-label text-left">Icon</label>
            <div class="icons col-md-10 text-left">
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon0" value="0" tabindex="-1" checked />
                    <label class="form-check-label ms-1" for="postIcon0">
                        Kein Icon
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon32" value="32" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon32">
                        <img src="./img/icons/icon2.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon40" value="40" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon40">
                        <img src="./img/icons/icon11.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon34" value="34" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon34">
                        <img src="./img/icons/icon4.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon33" value="33" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon33">
                        <img src="./img/icons/icon3.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon34" value="41" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon41">
                        <img src="./img/icons/icon12.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon2" value="2" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon2">
                        <img src="./img/icons/thumbsup.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon1" value="1" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon1">
                        <img src="./img/icons/thumbsdown.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon54" value="54" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon54">
                        <img src="./img/icons/pfeil.gif" />
                    </label>
                </div>

                <br>

                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon38" value="38" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon38">
                        <img src="./img/icons/icon8.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon35" value="35" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon35">
                        <img src="./img/icons/icon5.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon28" value="28" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon28">
                        <img src="./img/icons/icon9.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon42" value="42" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon42">
                        <img src="./img/icons/icon13.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon36" value="36" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon36">
                        <img src="./img/icons/icon6.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon39" value="39" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon39">
                        <img src="./img/icons/icon10.gif" />
                    </label>
                </div>
                <div class="form-check form-check-inline p-0 me-1">
                    <input class="form-check-input m-0" type="radio" name="icon" id="postIcon37" value="37" tabindex="-1" />
                    <label class="form-check-label ms-1" for="postIcon37">
                        <img src="./img/icons/icon7.gif" />
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 col-form-label text-left">Optionen</label>
            <div class="options col-sm-10">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="enableConvertUrls" id="enableConvertUrls" value="1" checked>
                    <label class="form-check-label" for="enableConvertUrls">[URL] Tags automatisch einfügen?</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="disableBbcode" id="disableBbcode" value="1">
                    <label class="form-check-label" for="disableBbcode">BB-Code deaktivieren?</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="disableSmilies" id="disableSmilies" value="1">
                    <label class="form-check-label" for="disableSmilies">Smilies deaktivieren?</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="disableEnhancement" id="disableEnhancement" value="1">
                    <label
                        class="form-check-label"
                        for="disableEnhancement"
                        title="Deaktiviert temporär automatisches Link-Wrapping und ähnliche spezielle Methoden, die das posten erleichtern sollen. Hilfreich, wenn zum Beispiel Code gepostet wird, oder Links die nicht in [URL], [IMG] etc. gewrapped werden sollen."
                    >Enhancement deaktivieren?</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="postText" class="col-md-2 col-form-label text-left"></label>
            <div class="col-md-10 d-flex flex-row align-items-start">
                <button type="button" name="submit" value="1" class="btn btn-primary d-block w-100 mb-2 mr-1">Eintragen</button>
                <button type="button" name="preview" value="1" class="btn btn-secondary d-block w-100 ml-1">Vorschau</button>
            </div>
        </div>
        <div class="row">
            <div class="log offset-md-2 col-md-10 d-none"></div>
        </div>
    </div>
    <div class="preview container bg-primary p-3 mt-5 mb-5 hide">
        <div class="row">
            <div class="col"></div>
        </div>
    </div>
</div>
`;
        let parsedTemplate = GeneralUtility.parseTemplateString(template);

        // Get objects of important DOM elements
        this._title = parsedTemplate.querySelector('input[name="title"]');
        this._message = parsedTemplate.querySelector('textarea[name="message"]');
        this._icon = parsedTemplate.querySelector('input[name="icon"]');
        this._enableConvertUrls = parsedTemplate.querySelector('input[name="enableConvertUrls"]');
        this._disableBbcode = parsedTemplate.querySelector('input[name="disableBbcode"]');
        this._disableSmilies = parsedTemplate.querySelector('input[name="disableSmilies"]');
        this._disableEnhancement = parsedTemplate.querySelector('input[name="disableEnhancement"]');
        this._submitButton = parsedTemplate.querySelector('button[name="submit"]');
        this._previewButton = parsedTemplate.querySelector('button[name="preview"]');
        this._previewContainer = parsedTemplate.querySelector('.preview');
        this._logContainer = parsedTemplate.querySelector('.log');
        this._container.append(parsedTemplate);

        // Preview button does nothing here, but needs to be setup by some actual class
        this._previewButton.addEventListener('click', e => {
            e.preventDefault();
        });

        // Initializes functions of the textarea
        this.initializeOnPaste();
        this.initializeRememberPostMessage();

        // Initialize basic functions of the editor
        this.initializeRteButtons();
        this.initializeCodeButton();
        this.initializeListButton();
        this.initializeTableButton();
        this.initializeSpoilerButton();
        this.initializeTriggerButton();
        this.initializeTexButton();
        this.initializeUrlButton();
        this.initializeUrlTextButton();
        this.initializeUrlImageButton();
        this.initializeImgButton();
        this.initializeVideoButton();
        this.initializeQuoteButton();
        this.initializeSmileySelect();
        this.initializeEmotes();
        this.initializeIcons();
        this.initializeFunctionCheckboxes();
    }


    /**
     * Initializes all functions related to pasteing a string into the textarea.
     *
     * @returns void
     */
    initializeOnPaste() {
        this._message.addEventListener('paste', e => {
            // Stop paste-enhancement if disabled via checkbox
            if (this._disableEnhancement.checked) {
                return;
            }

            e.preventDefault();

            let originalText = this._message.value;
            let paste = e.clipboardData.getData('text');
            let caretStartPosition = this._message.selectionStart;
            let caretEndPosition = this._message.selectionEnd;

            // Parse pasted string for bbcode
            let adjustedPaste = BbcodeUtility.parse(paste);

            // If a string has been selected by the caret, remove that string as it will be overwritten by the new paste
            if (caretStartPosition < caretEndPosition) {
                originalText = originalText.substr(0, caretStartPosition) + originalText.substr(caretEndPosition, originalText.length);
            }

            // Replace the original paste with the edited one
            let textBeforePaste = originalText.substr(0, caretStartPosition);
            let textAfterPaste = originalText.substr(caretStartPosition, originalText.length);

            this._message.value = textBeforePaste + adjustedPaste + textAfterPaste;
            TextareaUtility.putCaretAtPosition(this._message, caretStartPosition + adjustedPaste.length);
        });
    }


    /**
     * @inheritDoc
     */
    initializeDom() {
        super.initializeDom();
    }


    /**
     * Initializes the function to remember the typed in post message as a backup.
     *
     * @returns void
     */
    initializeRememberPostMessage() {
        const minTextLength = 20;
        let rememberPostMessageContainer = this._container.querySelector('.remember-post-message');
        let savedMessage = rememberPostMessageContainer.querySelector('.saved');
        let restoreMessage = rememberPostMessageContainer.querySelector('.restore');

        if (!Settings.getInstance().get('functions.rememberPostMessage')) {
            return;
        }

        // Show restore button, if some remembered text is available
        if (Settings.getInstance().get('functions.rememberPostMessageText')) {
            restoreMessage.classList.add('active');
            restoreMessage.addEventListener('click', () => {
                if (confirm('Replace all current content in textarea with remembered text?')) {
                    this._message.value = Settings.getInstance().get('functions.rememberPostMessageText');
                }
            });
        }

        // Save text on typing
        let typeDebounce = false;
        let allowedSpecialKeys = [
            8, // Backspace
            163, // #
            171, // *
            173, // -
            190, // .
            188, // ,
            59, // ü
            192, // ö
            222, // ä
            60, // <
            160, // ^
            63, // ?
            192 // ´
        ];

        this._message.addEventListener('keyup', e => {
            if (
                !(
                    (e.keyCode > 47 && e.keyCode < 58) || // Numbers
                    (e.keyCode > 95 && e.keyCode < 112) || // Numpad
                    (e.keyCode > 64 && e.keyCode < 91) || // Characters
                    allowedSpecialKeys.indexOf(e.keyCode) !== -1
                )
            ) {
                return
            }

            if (typeDebounce) {
                return;
            }
            typeDebounce = true;

            let text = this._message.value.trim();
            if (text && text.length > minTextLength) {
                Settings.getInstance().set('functions.rememberPostMessageText', text);
                GeneralUtility.addTimedClass(savedMessage, 'active', 1000);
            }

            setTimeout(() => {
                typeDebounce = false;
            }, 5000);
        });
    }


    /**
     * Initializes the RTE-like buttons.
     *
     * @returns {void}
     */
    initializeRteButtons() {
        let boldButton = this._container.querySelector('.functions .wrap-bold');
        boldButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[b]', '[/b]');
        });

        let underlineButton = this._container.querySelector('.functions .wrap-underline');
        underlineButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[u]', '[/u]');
        });

        let italicButton = this._container.querySelector('.functions .wrap-italic');
        italicButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[i]', '[/i]');
        });

        let strikeButton = this._container.querySelector('.functions .wrap-strike');
        strikeButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[s]', '[/s]');
        });

        let monospaceButton = this._container.querySelector('.functions .wrap-monospace');
        monospaceButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[m]', '[/m]');
        });
    }


    /**
     * Initializes the code button.
     *
     * @returns {void}
     */
    initializeCodeButton() {
        let codeButton = this._container.querySelector('.functions .insert-code');
        codeButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[code]', '[/code]');
        });
    }


    /**
     * Initializes the list button.
     *
     * @returns {void}
     */
    initializeListButton() {
        let listButton = this._container.querySelector('.functions .insert-list');
        listButton.addEventListener('click', () => {
            TextareaUtility.insertAtCaret(this._message, '[list]\n[*]\n[/list]');
        });
    }


    /**
     * Initializes the table button.
     *
     * @returns {void}
     */
    initializeTableButton() {
        let tableButton = this._container.querySelector('.functions .insert-table');
        tableButton.addEventListener('click', () => {
            TextareaUtility.insertAtCaret(this._message, '[table]\n1-1[||]1-2[--]\n2-1[||]2-2\n[/table]');
        });
    }


    /**
     * Initializes the spoiler button.
     *
     * @returns {void}
     */
    initializeSpoilerButton() {
        let spoilerButton = this._container.querySelector('.functions .insert-spoiler');
        spoilerButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[spoiler]', '[/spoiler]');
        });
    }


    /**
     * Initializes the trigger button.
     *
     * @returns {void}
     */
    initializeTriggerButton() {
        let triggerButton = this._container.querySelector('.functions .insert-trigger');
        triggerButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[trigger]', '[/trigger]');
        });
    }


    /**
     * Initializes the TeX button.
     *
     * @returns {void}
     */
    initializeTexButton() {
        let texButton = this._container.querySelector('.functions .insert-tex');
        texButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[tex]', '[/tex]');
        });
    }


    /**
     * Initializes the url-bbcode button.
     *
     * @returns {void}
     */
    initializeUrlButton() {
        let urlButton = this._container.querySelector('.functions .insert-url');
        urlButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[url]', '[/url]');
        });
    }


    /**
     * Initializes the url-bbcode button with a custom text as link.
     *
     * @returns {void}
     */
    initializeUrlTextButton() {
        let urlButton = this._container.querySelector('.functions .insert-url-text');
        urlButton.addEventListener('click', () => {
            let url = prompt('URL eingeben:');
            if (!url) {
                return;
            }

            let text = prompt('Text eingeben:');
            if (!text) {
                return;
            }

            TextareaUtility.wrapSelection(this._message, '[url=' + url + ']' + text, '[/url]');
        });
    }


    /**
     * Initializes the url-bbcode with an image as link body.
     *
     * @returns {void}
     */
    initializeUrlImageButton() {
        let urlButton = this._container.querySelector('.functions .insert-url-image');
        urlButton.addEventListener('click', () => {
            let url = prompt('URL eingeben:');
            if (!url) {
                return;
            }

            let imageUrl = prompt('URL zu einem Bild eingeben:');
            if (!imageUrl) {
                return;
            }

            TextareaUtility.wrapSelection(this._message, '[url=' + url + '][img]' + imageUrl + '[/img]', '[/url]');
        });
    }


    /**
     * Initializes the img-bbcode button.
     *
     * @returns {void}
     */
    initializeImgButton() {
        let imgButton = this._container.querySelector('.functions .insert-img');
        imgButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[img]', '[/img]');
        });
    }


    /**
     * Initializes the video-bbcode button.
     *
     * @returns {void}
     */
    initializeVideoButton() {
        let videoButton = this._container.querySelector('.functions .insert-video');
        videoButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[video]', '[/video]');
        });
    }


    /**
     * Initializes the quote button.
     *
     * @returns {void}
     */
    initializeQuoteButton() {
        let quoteButton = this._container.querySelector('.functions .insert-quote');
        quoteButton.addEventListener('click', () => {
            TextareaUtility.wrapSelection(this._message, '[quote]', '[/quote]');
        });
    }


    /**
     * Initializes the smiley selection.
     *
     * @returns {void}
     */
    initializeSmileySelect() {
        let smileyContainer = this._container.querySelector('.smilies');
        new bootstrap.Dropdown(smileyContainer.querySelector('button'));
        let smileyDropdown = smileyContainer.querySelector('.dropdown-menu');
        let smileyOptions = smileyDropdown.querySelectorAll('.smiley');

        smileyOptions.forEach(option => {
            option.addEventListener('click', () => {
                let shortcut = option.querySelector('.shortcut').innerHTML;
                TextareaUtility.insertAtCaret(this._message, shortcut);
            });
        });
    }


    /**
     * Initializes the emote selection.
     *
     * @returns {void}
     */
    initializeEmotes() {
        // Check if any emotes are present
        let emotes = Settings.getInstance().get('emotes');
        let emoteKeys = Object.keys(emotes);
        if (emoteKeys.length == 0) {
            return;
        }

        // Build emote dropdown
        let emotesContainer = this._container.querySelector('.emotes');
        emotesContainer.classList.remove('d-none');
        new bootstrap.Dropdown(emotesContainer.querySelector('button'));
        let emotesDropdown = emotesContainer.querySelector('.dropdown-menu');

        emoteKeys.forEach(key => {
            let emote = emotes[key];

            let dropdownItem = document.createElement('a');
            dropdownItem.classList.add('dropdown-item', 'emote');

            let imageContainer = document.createElement('span');
            imageContainer.classList.add('image');

            let image = document.createElement('img');
            image.setAttribute('src', emote.url);

            let shortcutContainer = document.createElement('span');
            shortcutContainer.classList.add('shortcut');

            imageContainer.append(image);
            dropdownItem.append(imageContainer, shortcutContainer);

            emotesDropdown.append(dropdownItem);

            // Insert emote into textarea on click
            dropdownItem.addEventListener('click', e => {
                TextareaUtility.insertAtCaret(this._message, emote.identifier);
            });
        });
    }


    /**
     * Initializes the post icons.
     *
     * @returns {void}
     */
    initializeIcons() {
    }


    /**
     * Initializes the checkboxes enabling or disabling extended functionality of the RTE.
     *
     * @returns {void}
     */
    initializeFunctionCheckboxes() {
        this._disableEnhancement.addEventListener('click', () => {
            this._enableConvertUrls.checked = !this._disableEnhancement.checked;
            this._disableBbcode.checked = this._disableEnhancement.checked;
            this._disableSmilies.checked = this._disableEnhancement.checked;
        });
    }


    /**
     * Disables the editor.
     *
     * @returns {void}
     */
    disable() {
        this._container.classList.add('disable');
    }


    /**
     * Enables the editor.
     *
     * @returns {void}
     */
    enable() {
        this._container.classList.remove('disable');
    }


    /**
     * Validates the form input.
     *
     * @returns {boolean} True when the form was filled out correctly, otherwise false
     */
    validate() {
        return true;
    }


    /**
     * Adds a log message.
     *
     * @param {string} level Log level (either 'information', 'warning' or 'error', defaults to 'info')
     * @param {string} message Log message
     *
     * @returns {void}
     */
    addLogMessage(level, message) {
        let alert = this.createLogMessage(level, message);
        this._logContainer.classList.remove('d-none');
        this._logContainer.appendChild(alert);
    }


    /**
     * Creates a log message.
     *
     * @param {string} level Log level (either 'information', 'warning' or 'error', defaults to 'info')
     * @param {string} message Log message
     *
     * @returns {Element}
     */
    createLogMessage(level, message) {
        let alertClass = 'alert-info';
        if (level === 'warning') {
            alertClass = 'alert-warning';
        } else if (level === 'error') {
            alertClass = 'alert-danger';
        }

        var alert = document.createElement('div');
        alert.classList.add('alert', alertClass);
        alert.innerHTML = message;

        return alert;
    }


    /**
     * Clears all current log messages.
     *
     * @returns {void}
     */
    clearLogMessages() {
        this._logContainer.innerHTML = '';
        this._logContainer.classList.add('d-none');
    }


    /**
     * Loads HTML into the preview container.
     *
     * @param {string} html HTML code of the preview
     * @param {string} [title] Post title (defaults to nothing)
     * @param {string} [icon] Post icon (defaults to nothing)
     */
    loadPreview(html, title, icon) {
        GeneralUtility.removeSpinners(this._previewContainer);
        this._previewContainer.classList.remove('hide', 'pulse');

        let container = this._previewContainer.querySelector('.col');
        container.innerHTML = '';

        // Prepare icon and title
        let hasIcon = icon !== undefined && icon && icon !== '0';
        let hasTitle = title !== undefined && title;

        if (hasIcon) {
            let iconImage = document.querySelector('.editor .icons input[value="' + icon + '"] + label img');
            if (iconImage) {
                icon = '<img src="' + iconImage.getAttribute('src') + '" alt="Post Icon">';
            }
        } else {
            icon = '';
        }

        if (hasTitle) {
            title = '<span>' + title + '</span>';
        } else {
            title = '';
        }

        // Add icon and title as needed
        if (icon || title) {
            container.innerHTML += '<h5 class="title mb-4">' + icon + title + '</h5>';
        }

        container.innerHTML += html;
    }


    /**
     * Adds final parsing to the textarea message.
     *
     * What this method does:
     * - Parse emote shortcuts into URLs
     *
     * What this method does NOT do:
     * - Parse smilies (is done by the server).
     * - Parse bbcode (is done when pasting text into the textarea).
     *
     * @params {string} message Post message to parse
     * @returns {string} Parsed final message
     */
    parseFinalMessage(message) {
        // Stop parse-enhancement if disabled via checkbox
        if (this._disableEnhancement.checked) {
            return message;
        }

        // Check if any emotes are present
        let emotes = Settings.getInstance().get('emotes');
        let emoteKeys = Object.keys(emotes);
        if (emoteKeys.length == 0) {
            return message;
        }

        emoteKeys.forEach(key => {
            let emote = emotes[key];

            // Replace emote identifier with actual URL
            message = message.replace(emote.identifier, '[img]' + emote.url + '[/img]');
        });

        return message;
    }
}
