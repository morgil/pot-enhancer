/**
 * View for applying the user block list.
 *
 * @author FiveFuenf
 */
class BlockUsers extends AbstractView {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this.initializeDom();
    }


    /**
     * @override
     */
    render() {
        super.render();
    }


    /**
     * @override
     */
    initializeDom() {
        super.initializeDom();

        let blockedUsernames = Object.keys(Settings.getInstance().get('blockUsers'));
        if (blockedUsernames.length === 0) {
            return;
        }

        // Block users by the data-username attribute in the tr-elements
        if (Router.activeRoute instanceof ThreadRoute) {
            let posts = DomUtility.posts;
            blockedUsernames.forEach(username => {
                let postsFromBlockedUsers = GeneralUtility.filterByAttribute(posts, 'username', escape(username));
                postsFromBlockedUsers.forEach(post => {
                    post.nextElementSibling.remove();
                    post.remove();
                });
            });
        }

        // Block users by searching for the username in the post-preview
        else if (Router.activeRoute instanceof CreatePostRoute) {
            let previewPosts = DomUtility.previewPosts;
            previewPosts.forEach(previewPost => {
                let username = previewPost.querySelector('a[onclick*="openProfile"]');
                if (!username) {
                    return;
                }

                username = username.innerHTML.trim();
                if (blockedUsernames.indexOf(username) !== -1) {
                    previewPost.remove();
                }
            });
        }
    }
}
