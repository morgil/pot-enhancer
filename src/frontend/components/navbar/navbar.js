/**
 * View for adjusting main navbar.
 *
 * @author FiveFuenf
 */
class Navbar extends AbstractView {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this.initializeDom();
    }


    /**
     * @override
     */
    render() {
        super.render();
    }


    /**
     * @override
     */
    initializeDom() {
        super.initializeDom();

        // Add links to other useful pOT-Service
        let target = DomUtility.navbar.querySelector('td[align="right"]');

        let spacer1 = document.createElement('span');
        spacer1.classList.add('separator');
        spacer1.innerHTML = ' | ';

        let spacer2 = document.createElement('span');
        spacer2.classList.add('separator');
        spacer2.innerHTML = ' | ';

        // pOT search
        let searchLink = document.createElement('a');
        searchLink.classList.add('external-service');
        searchLink.setAttribute('href', 'https://potstats2.enkore.de/search/');
        searchLink.setAttribute('target', '_blank;');
        searchLink.innerHTML = 'pOTsearch';

        // pOT statistics
        let statisticsLink = document.createElement('a');
        statisticsLink.classList.add('external-service');
        statisticsLink.setAttribute('href', 'https://potstats2.enkore.de/');
        statisticsLink.setAttribute('target', '_blank;');
        statisticsLink.innerHTML = 'pOTStats';

        target.prepend(statisticsLink, spacer1, searchLink, spacer2);
    }
}
