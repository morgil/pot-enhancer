/**
 * Base class of a route.
 *
 * @author FiveFuenf
 */
class Route {
    constructor() {
        this._url = window.location.href;
    }


    /**
     * Gets one or multiple regular expressions that match if the current
     * URL fit this route.
     *
     * @returns {Array} RegExp's matching this route
     */
    static get matchers() {
        let matchers = [];
        return matchers;
    }


    /**
     * Checks if this route is currently active. If any register matcher
     * (see Route.matchers) will match the current URL, this method will return
     * true.
     *
     * @returns {boolean} True when any matcher matches, otherwise false
     */
    isActive() {
        return false;
    }
}
