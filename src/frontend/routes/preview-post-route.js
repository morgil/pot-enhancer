/**
 * Route for previewing a post.
 *
 * @author FiveFuenf
 */
class PreviewPostRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+editreply.php\?action=preview$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < PreviewPostRoute.matchers.length; i++) {
            if (PreviewPostRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Get rendered preview
        let preview = DomUtility.previewContainer.innerHTML;
    }
}
