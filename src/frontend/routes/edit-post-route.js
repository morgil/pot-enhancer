/**
 * Route for editing a post.
 *
 * @author FiveFuenf
 */
class EditPostRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+editreply.php\?PID=\d+$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < EditPostRoute.matchers.length; i++) {
            if (EditPostRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Adjust navbar
        let navbar = new Navbar();

        // Block users
        if (!GeneralUtility.isEmpty(Settings.getInstance().get('blockUsers'))) {
            let blockUsers = new BlockUsers();
        }

        // Setup custom editor
        DomUtility.postFormContainer.classList.add('d-none');
        let editorContainer = document.createElement('div');
        GeneralUtility.insertBefore(editorContainer, DomUtility.postFormContainer);
        let editor = new PostEditor(editorContainer);
    }
}
