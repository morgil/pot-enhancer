/**
 * Route for creating a new thread.
 *
 * @author FiveFuenf
 */
class CreateThreadRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+newthread.php\?BID=\d+&SID=.*$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < CreateThreadRoute.matchers.length; i++) {
            if (CreateThreadRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Adjust navbar
        let navbar = new Navbar();

        // Setup custom editor
        DomUtility.postFormContainer.classList.add('d-none');
        let editorContainer = document.createElement('div');
        GeneralUtility.insertBefore(editorContainer, DomUtility.postFormContainer);
        let editor = new ThreadEditor(editorContainer);
    }
}
