/**
 * Route for a thread (post listing).
 *
 * @author FiveFuenf
 */
class ThreadRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+thread.php\?TID=\d+/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < ThreadRoute.matchers.length; i++) {
            if (ThreadRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Adjust navbar
        let navbar = new Navbar();

        // Setup general posts component
        let posts = new Posts();

        // Setup quick-post editor
        this.initializeQuickPostEditor();

        // Block users
        if (!GeneralUtility.isEmpty(Settings.getInstance().get('blockUsers'))) {
            let blockUsers = new BlockUsers();
        }
    }


    /**
     * Initializes the quick post editor.
     *
     * @returns {void}
     */
    initializeQuickPostEditor() {
        // Create container for the quick post editor
        let editorContainer = document.createElement('div');
        editorContainer.setAttribute('id', 'quickPostEditor');
        editorContainer.classList.add('collapse');

        let editor = new PostEditor(editorContainer);

        if (DomUtility.threadTags) {
            GeneralUtility.insertBefore(editorContainer, DomUtility.threadTags);
        } else {
            GeneralUtility.insertAfter(editorContainer, DomUtility.postsPagination);
        }

        // Create button for toggling the quick-reply container
        let newThreadButton = DomUtility.postsPagination.querySelector('a[href^="newthread"]');
        if (!newThreadButton) {
            newThreadButton = DomUtility.postsPagination.querySelector('img[src$="nonewthread.gif"]');
        }

        let quickReplyImage = document.createElement('img');
        quickReplyImage.setAttribute('src', browser.runtime.getURL('dist/resources/images/quick-reply-button.png'));

        let quickReplyButton = document.createElement('a');
        quickReplyButton.setAttribute('href', '#quickPostEditor');
        quickReplyButton.setAttribute('data-bs-toggle', 'collapse');
        quickReplyButton.setAttribute('role', 'button');
        quickReplyButton.setAttribute('aria-expanded', false);
        quickReplyButton.setAttribute('aria-controls', 'quickPostEditor');
        quickReplyButton.classList.add('quick-reply-button', 'mx-3');
        quickReplyButton.append(quickReplyImage);

        GeneralUtility.insertBefore(quickReplyButton, newThreadButton);

        // Initialize bootstrap collapsible
        new bootstrap.Collapse(quickReplyButton);

        // Scroll down when opening the quick post editor
        editorContainer.addEventListener('shown.bs.collapse', () => {
            DomUtility.scrollingElement.scrollTop = DomUtility.scrollingElement.offsetHeight;
        });
    }
}
