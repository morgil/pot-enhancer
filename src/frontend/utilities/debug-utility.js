/**
 * Provides debugging utilities.
 *
 * @author FiveFuenf
 */
class DebugUtility {
    constructor() {
    }


    /**
     * Gets if the debugging is enabled.
     *
     * @returns {boolean} True when enabled, otherwise false
     */
    static isEnabled() {
        return DebugUtility._isEnabled;
    }


    /**
     * Sets if debugging should be enabled.
     *
     * @param {boolean} isEnabled True when enabled, otherwise false
     *
     * @returns {void}
     */
    static setIsEnabled(isEnabled) {
        DebugUtility._isEnabled = isEnabled;
    }


    /**
     * Standard debug log.
     *
     * @param {*} message Message to log
     * @param {boolean} [force=false] Whether to force logging despite the isEnabled flag
     *
     * @returns {void}
     */
    static log(message, force) {
        if (force === undefined) {
            force = false;
        }

        if (console !== undefined && (DebugUtility.isEnabled() || force)) {
            console.log(message);
        }
    }


    /**
     * Warning log.
     *
     * @param {*} message Message to log
     * @param {boolean} [force=false] Whether to force logging despite the isEnabled flag
     *
     * @returns {void}
     */
    static warn(message, force) {
        if (force === undefined) {
            force = false;
        }

        if (console !== undefined && (DebugUtility.isEnabled() || force)) {
            console.warn(message);
        }
    }


    /**
     * Error log.
     *
     * @param {*} message Message to log
     * @param {boolean} [force=false] Whether to force logging despite the isEnabled flag
     *
     * @returns {void}
     */
    static error(message, force) {
        if (force === undefined) {
            force = false;
        }

        if (console !== undefined && (DebugUtility.isEnabled() || force)) {
            console.error(message);
        }
    }
}

// Contains the debug-enabled state
DebugUtility._isEnabled = false;
