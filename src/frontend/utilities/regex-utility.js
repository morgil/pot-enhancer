/**
 * Utility class dealing with common RegEx features.
 *
 * @author FiveFuenf
 */
class RegExUtility {
    constructor() {
    }


    /**
     * Escapes a string so it can be replaced via str.replace(regex) for example.
     *
     * @param {string} string String to escape
     * @returns {string} Escaped string
     */
    static escape(string) {
        return string.replace(new RegExp(/([\?\[\]\(\)\+\$\^]+)/, 'g'), '\\$1');
    }


    /**
     * Finds matches with the given regular expression in the given string and returns the captured groups.
     * The given regular expression needs to be global, if all captured strings should be found, not just the first one.
     *
     * @param {string} string String to search through
     * @param {RegExp} regExp Regular expression with capture groups to perform on the string
     *
     * @returns {Array} A distinct list of matches strings
     */
    static find(string, regExp) {
        let distinctMatches = [];

        // match[0]: Value that was searched for
        // match[1 ... n]: Contains found values (for each capture group)
        // match.length: Equals number of found matches +1
        // match.input: Initial input for the RegEx
        let matches = regExp.exec(string);

        // If this is not a global search, directly return first capture group
        // Otherwise the while loop will never terminate
        if (!regExp.global) {
            if (matches.length > 0) {
                distinctMatches.push(matches[1]);
            }
            return distinctMatches;
        }

        while (matches !== null) {
            // There were matches starting at index 1
            if (matches.length > 0) {
                for (let i = 1; i < matches.length; i++) {
                    if (matches[i] !== undefined && matches[i] && distinctMatches.indexOf(matches[i]) === -1) {
                        distinctMatches.push(matches[i]);
                    }
                }
            }

            matches = regExp.exec(string);
        }

        return distinctMatches;
    }


    /**
     * Finds all image urls.
     *
     * @param {string} string String to match
     *
     * @returns {Array} Array of found URLs
     */
    static findImageUrls(string) {
        return RegExUtility.find(string, new RegExp(/(https?:\/\/[^\s]+\.(?:jpg|jpeg|png|gif|bmp|webm)[^\s\[]*)/, 'gi'));
    }


    /**
     * Finds all Imgur URLs. This does not include fully qualified URLs, like "https://i.imgur.com/MWE8fI4.png", as that case is covered by the
     * findImageUrl-method. This method finds all URLs like "https://imgur.com/e6i3nox".
     *
     * @param {string} string String to match
     *
     * @returns {Array} Array of found URLs
     */
    static findImgurUrls(string) {
        return RegExUtility.find(string, new RegExp(/(?!.*(jpg|jpeg|png|gif|bmp|webm))(https?:\/\/(www)?imgur\.com\/[a-zA-Z\d]+)/, 'gi'));
    }


    /**
     * Finds all Imgur URLs like "https://i.imgur.com/MWE8fI4.png", not however links like "https://i.imgur.com/MWE8fI4" (as opposed to the findImgurUrls()
     * method).
     *
     * @param {string} string String to match
     *
     * @returns {Array} Array of found URLs
     */
    static findImgurUrlsWithFileExtension(string) {
        return RegExUtility.find(string, new RegExp(/(https?:\/\/(www)?imgur\.com\/[a-zA-Z\d]+\.[a-zA-Z\d]+)/, 'gi'));
    }


    /**
     * Finds all audio urls.
     *
     * @param {string} string String to match
     *
     * @returns {Array} Array of found URLs
     */
    static findAudioUrls(string) {
        return RegExUtility.find(string, new RegExp(/(https?:\/\/[^\s]+\.(?:mp3|wav|flac|ogg)[^\s\[]*)/, 'gi'));
    }


    /**
     * Finds all video urls.
     *
     * @param {string} string String to match
     *
     * @returns {Array} Array of found URLs
     */
    static findVideoUrls(string) {
        return RegExUtility.find(string, new RegExp(/(https?:\/\/[^\s]+\.(?:mp4|mpeg|mov|mpg|mkv|avi)[^\s\[]*)/, 'gi'));
    }


    /**
     * Finds all Youtube urls.
     *
     * @param {string} string String to match
     *
     * @returns {Array} Array of found URLs
     */
    static findYoutubeUrls(string) {
        return RegExUtility.find(string, new RegExp(/(https?:\/\/(?:www\.|i\.)?youtube.com\/watch\?v=[^\s\[]*)/, 'gi'));
    }


    /**
     * Extracts a thread ID from the given URL.
     *
     * @param {string} string URL
     *
     * @returns {string} Thread ID, if found
     */
    static extractTidFromUrl(string) {
        let matches = RegExUtility.find(string, new RegExp(/^.*TID=(\d+).*$/, 'gi'));
        if (matches.length !== 0) {
            return matches[0];
        }
        return '';
    }
}
