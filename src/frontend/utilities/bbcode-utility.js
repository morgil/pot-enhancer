/**
 * Contains functions for dealing with bbcode stuff.
 *
 * @author FiveFuenf
 */
class BbcodeUtility {
    constructor() {
    }


    /**
     * Parses a string to insert fitting bbcode.
     * Basically turns any string into a string with bbcode functions where fitting. Stuff already in bbcode-form is not touched.
     *
     * @param {string} string String to adjust
     *
     * @returns {string} Parsed string where all fitting strings have been enriched with bbcode
     */
    static parse(string) {
        string = BbcodeUtility.parseImg(string);
        string = BbcodeUtility.parseVideo(string);
        string = BbcodeUtility.parseAudio(string);
        return string;
    }


    /**
     * Parses a text to turn image links into links wrapped in [img].
     * Links already wrapped in [img] are ignored.
     *
     * Supports:
     * - Wrap image links in [img]
     * - Wrap imgur links in [img]
     *
     * @param {string} string String to adjust
     *
     * @returns {string} Parsed string with bbcode features
     */
    static parseImg(string) {
        string = BbcodeUtility.removeTags(string, 'img');

        // Add an arbitrary file extension to all imgur links, that were pasted without file extension; otherwise the [img]-tag won't render the images
        // Take extra care for imgur links that are present as duplicates: once with file extension, once without. In that case, the variant with file
        // extension will loose its file extension, so that double-file-extension-replacement is prevented.
        let imgurUrls = RegExUtility.findImgurUrls(string);
        let imgurUrlsWithFileExtension = RegExUtility.findImgurUrlsWithFileExtension(string);

        for (let i = 0; i < imgurUrls.length; i++) {
            let match = imgurUrls[i];

            for (let j = 0; j < imgurUrlsWithFileExtension.length; j++) {
                let matchWithFileExtension = imgurUrlsWithFileExtension[j];

                // There is a duplicate imgur URL with a file extension; replace with variant witout file extension
                if (matchWithFileExtension.indexOf(match) !== -1) {
                    string = string.replaceAll(matchWithFileExtension, match);
                }
            }

            // Add arbitrary png-file-extension to force rendering in [img]-tag
            string = string.replaceAll(match, match + '.png');
        }

        let imageUrls = RegExUtility.findImageUrls(string);
        string = BbcodeUtility.wrapInTag(imageUrls, string, 'img');

        return string;
    }


    /**
     * Parses a text to turn video links into links wrapped in [video].
     * Links already wrapped in [video] are ignored.
     *
     * Supports:
     * - Wrap video links in [video]
     * - Wrap YouTube links in [video]
     *
     * @param {string} string String to adjust
     *
     * @returns {string} Parsed string with bbcode features
     */
    static parseVideo(string) {
        string = BbcodeUtility.removeTags(string, 'video');

        let videoUrls = RegExUtility.findVideoUrls(string);
        string = BbcodeUtility.wrapInTag(videoUrls, string, 'video');

        let youtubeUrls = RegExUtility.findYoutubeUrls(string);
        string = BbcodeUtility.wrapInTag(youtubeUrls, string, 'video');

        // Quoted posts with videos contain the video URL in an [url]-tag, not the original [video]-tag. When inserting such content in the textarea, the
        // creation of double-tags like [url][video]foo[/video][/url] needs to be prevented. Remove any double-tags after all replacement is done.
        string = string.replaceAll('[url][video]', '[url]');
        string = string.replaceAll('[/video][/url]', '[/url]');
        return string;
    }


    /**
     * Parses a text to turn video links into links wrapped in [audio].
     * Links already wrapped in [audio] are ignored.
     *
     * Supports:
     * - Wrap audio links in [audio]
     *
     * @param {string} string String to adjust
     *
     * @returns {string} Parsed string with bbcode features
     */
    static parseAudio(string) {
        string = BbcodeUtility.removeTags(string, 'audio');

        var audioUrls = RegExUtility.findAudioUrls(string);
        string = BbcodeUtility.wrapInTag(audioUrls, string, 'audio');

        return string;
    }


    /**
     * Removes a bbcode tag from a string.
     *
     * @param {string} string String to adjust
     * @param {string} tag Tag to remove, e.g. "img" to remove all [img] and [/img] occurences
     *
     * @returns {string} Text without any bbcode tags
     */
    static removeTags(string, tag) {
        string = string.replace(new RegExp('\\[' + tag + '\\]', 'gi'), '');
        string = string.replace(new RegExp('\\[\\/' + tag + '\\]', 'gi'), '');
        return string;
    }


    /**
     * Wraps all matches of a string with the given bbcode tag.
     *
     * @param {Array} matches Matches to wrap in the string
     * @param {string} string String being adjusted
     * @param {string} tag Tag to use, e.g. 'img'
     * @param {string} [prepend=''] Prepend a string to the wrapped string
     * @param {string} [append=''] ppend a string to the wrapped string
     *
     * @returns {string} Adjusted string with wrapped substrings
     */
    static wrapInTag(matches, string, tag, prepend, append) {
        if (prepend === undefined) {
            prepend = '';
        }
        if (append === undefined) {
            append = '';
        }

        for (let i = 0; i < matches.length; i++) {
            let match = matches[i];
            var escapedMatch = RegExUtility.escape(match);
            string = string.replace(new RegExp(escapedMatch, 'gi'), '[' + tag + ']' + prepend + match + append + '[/' + tag + ']');
        }

        return string;
    }
}
