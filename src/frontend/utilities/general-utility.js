/**
 * Contains general functions.
 *
 * @author FiveFuenf
 */
class GeneralUtility {
    constructor() {
    }


    /**
     * Pushes the given content as a download to the client.
     *
     * @param {*} content Content to download
     * @param {string} filename Filename for holding the content
     * @param {string} mimeType What type of download
     *
     * @returns {Promise}
     *
     * @throws When the browser object is missing
     */
    static download(content, filename, mimeType) {
        if (browser === undefined) {
            throw new Error('Cannot download, as "browser" is undefined.');
        }

        let file = new Blob([content], {'type': mimeType});
        let url = URL.createObjectURL(file);

        let downloading = browser.downloads.download({
            'url': url,
            'filename': filename
        });

        return downloading;
    }


    /**
     * Checks if a given filename fulfills the given file extension.
     *
     * @param {string} filename Filename to check, e.g. "foo.json"
     * @param {string} extension Extension that the file should be, eg "json"
     *
     * @returns {boolean} True when the filename fulfills the given extensions, otherwise false
     */
    static isFileExtension(filename, extension) {
        let parts = filename.split('.');
        if (parts.length < 2) {
            return false;
        }
        return parts[parts.length - 1] === extension;
    }


    /**
     * Checks if an object is empty.
     *
     * @param {Object} object Object to check
     *
     * @returns {boolean} True when the object is empty, otherwise false
     */
    static isEmpty(object) {
        if (object === undefined || !object) {
            return true;
        }
        return Object.keys(object).length === 0;
    }


    /**
     * Checks if a string is empty.
     * A string is considered empty, if it only contains whitespace or is simply
     * not set.
     *
     * @param {string} string String to check
     *
     * @returns {boolean} True when the string is empty, otherwise false
     */
    static isStringEmpty(string) {
        if (string === undefined ||
            !string ||
            string.trim().length === 0) {
            return true;
        }
        return false;
    }


    /**
     * Finds direct children of a parent fitting the given query selector.
     *
     * @param {Element} parent Parent
     * @param {string} query Query that the children have to match
     *
     * @returns {Array} Children
     */
    static findChildren(parent, query) {
        return Array.prototype.filter.call(parent.children, (child) => {
            return child.matches(query);
        });
    }


    /**
     * Inserts an elements before a given reference element.
     *
     * @param {Element} element Element to insert
     * @param {Element} referenceElement Reference element
     */
    static insertBefore(element, referenceElement) {
        referenceElement.parentNode.insertBefore(element, referenceElement);
    }


    /**
     * Inserts an elements after a given reference element.
     *
     * @param {Element} element Element to insert
     * @param {Element} referenceElement Reference element
     */
    static insertAfter(element, referenceElement) {
        referenceElement.parentNode.insertBefore(element, referenceElement.nextSibling);
    }


    /**
     * Filters the given node list by the given class name.
     *
     * @param {NodeList} nodeList Node list to filter
     * @param {string} className Class name to filter for
     *
     * @returns {NodeList} Nodes that fit the class name
     */
    static filterByClassName(nodeList, className) {
        return [...nodeList].filter(node => {
            return node.classList.contains(className);
        });
    }


    /**
     * Filters the given node list by the selected attribute and value.
     *
     * @param {NodeList} nodeList Node list to filter
     * @param {NodeList} attribute Attribute to filter for
     * @param {string} value Filter value for the attribute
     *
     * @returns {NodeList} Nodes that fit the attribute
     */
    static filterByAttribute(nodeList, attribute, value) {
        return [...nodeList].filter(node => {
            return node.getAttribute(attribute) == value;
        });
    }


    /**
     * Adds classes to all elements in the node list.
     *
     * @param {NodeList} nodeList Node list to add classes to
     * @param {string|Array} classNames Class names to add (may be a string of only a single class is added)
     *
     * @returns {void}
     */
    static addClasses(nodeList, classNames) {
        if (!Array.isArray(classNames)) {
            classNames = [classNames];
        }

        nodeList.forEach(node => {
            node.classList.add(classNames);
        });
    }


    /**
     * Removes classes from all elements in the node list.
     *
     * @param {NodeList} nodeList Node list to remove classes from
     * @param {string|Array} classNames Class names to remove (may be a string of only a single class is removed)
     *
     * @returns {void}
     */
    static removeClasses(nodeList, classNames) {
        if (!Array.isArray(classNames)) {
            classNames = [classNames];
        }

        nodeList.forEach(node => {
            node.classList.remove(classNames);
        });
    }


    /**
     * Parses a template string into a DOM tree.
     *
     * @param {string} templateString Template to parse
     * @param {string} [type='text/html'] Desired template type
     *
     * @returns {Document}
     */
    static parseTemplateString(templateString, type) {
        if (type === undefined) {
            type = 'text/html';
        }

        let parser = new DOMParser();
        let dom = parser.parseFromString(templateString, type);

        // DOMParser returns a complete, fresh HTMLDocument instance; however only the body children are needed
        // Wrap in placeholder element and then return only the children
        let wrap = document.createElement('div');
        for (let child of dom.body.children) {
            wrap.append(child);
        }

        return wrap;
    }


    /**
     * Creates a deep clone of the given object.
     *
     * @param {Object} object Object to clone
     *
     * @returns {Object} Cloned object
     */
    static clone(object) {
        return JSON.parse(JSON.stringify(object));
    }


    /**
     * Gets a specific GET parameters of the current request.
     *
     * @param {string} key Name of the GET parameter to read
     *
     * @returns {*|null} Value of the GET parameter, if found
     */
    static getRequestParameter(key) {
        var url = new URL(window.location.href);
        if (url.searchParams.has(key)) {
            return url.searchParams.get(key);
        }
        return null;
    }


    /**
     * Gets a the postId from the URL hash, if available.
     *
     * @returns {string|null} PostID if a valid hash is present
     */
    static getPostIdFromHash() {
        var url = new URL(window.location.href);
        if (!url.hash) {
            return null;
        }

        let hash = RegExUtility.find(url.hash, new RegExp('^#reply_([0-9]+)$', 'g'));
        if (hash.length === 0) {
            return null;
        }

        return hash[0];
    }


    /**
     * Gets the currently active HTTP protocol.
     *
     * @param {boolean} [addSlashes=true] Whether to add "://" automatically
     *
     * @returns {string} http or https
     */
    static getProtocol(addSlashes) {
        if (addSlashes === undefined) {
            addSlashes = true;
        }

        let protocol = 'http';
        if (location.protocol.indexOf('s') !== -1) {
            protocol = 'https';
        }

        if (addSlashes) {
            protocol += '://';
        }

        return protocol;
    }


    /**
     * Gets the base URL form the current URL.
     *
     * @returns {string}
     */
    static getBaseUrl() {
        let url = window.location.href;
        url = url.split('?');
        return url[0];
    }


    /**
     * Builds an URL to the given board.
     *
     * @param {int} boardId ID of the board to create URL for
     * @param {int} [page=0] Number of the page to link to
     *
     * @returns {string}
     */
    static buildUrlToBoard(boardId, page) {
        if (page === undefined) {
            page = 0;
        }

        let url = GeneralUtility.getProtocol() + 'forum.mods.de/bb/board.php?BID=' + boardId;

        if (page > 0) {
            url += '&page=' + page;
        }

        return url;
    }


    /**
     * Redirects the client to the given board
     *
     * @param {int} boardId ID of the board to redirect to
     *
     * @returns {void}
     */
    static redirectToBoard(boardId) {
        window.location.href = GeneralUtility.buildUrlToBoard(boardId);
    }


    /**
     * Builds an URL to the given thread.
     *
     * @param {int} boardId ID of the thread to create URL for
     * @param {int} [page=false] Number of page to link to (use -1 to link to last post in thread)
     *
     * @returns {string}
     */
    static buildUrlToThread(threadId, page) {
        if (page === undefined) {
            page = false;
        }

        let url = GeneralUtility.getProtocol() + 'forum.mods.de/bb/thread.php?TID=' + threadId;

        if (page === -1) {
            url += '&last=1#last_reply';
        } else if (page > 0) {
            url += '&page=5';
        }

        return url;
    }


    /**
     * Redirects the client to the given thread
     *
     * @param {int} threadId ID of the thread to redirect to
     * @param {int} [page] Number of page to link to (use -1 to link to last post in thread)
     *
     * @returns {void}
     */
    static redirectToThread(threadId, page) {
        let url = GeneralUtility.buildUrlToThread(threadId, page);

        // If the URL has a hash and it stayed the same as the current browser URL, window.location.href will not trigger a reload
        // Use window.location.reload in that case
        if (url.indexOf('#') !== -1 && url === window.location.href) {
            DebugUtility.log('URL with hash stayed the same, use window.location.reload().');
            window.location.reload(true);
            return;
        }

        window.location.href = url;
    }


    /**
     * Builds an URL to the given post.
     *
     * @param {int} postId ID of the post to jump to
     * @param {int} threadId ID of the thread to create URL for
     *
     * @returns {string}
     */
    static buildUrlToPost(postId, threadId) {
        return GeneralUtility.getProtocol() + 'forum.mods.de/bb/thread.php?TID=' + threadId + '&PID=' + postId + '#reply_' + postId;
    }


    /**
     * Redirects the client to the given post.
     *
     * @param {int} postId ID of the post to jump to
     * @param {int} threadId ID of the thread to create URL for
     *
     * @returns {string}
     */
    static redirectToPost(postId, threadId) {
        let url = GeneralUtility.buildUrlToPost(postId, threadId);

        // If the URL has a hash and it stayed the same as the current browser URL, window.location.href will not trigger a reload
        // Use window.location.reload in that case
        if (url.indexOf('#') !== -1 && url === window.location.href) {
            DebugUtility.log('URL with hash stayed the same, use window.location.reload().');
            window.location.reload(true);
            return;
        }

        window.location.href = url;
    }


    /**
     * Creates a FormData object from a JSON object.
     * This method is used to send data to the server.
     *
     * @param {JSON} json JSON to convert to FormData
     *
     * @returns {FormData}
     */
    static createFormData(json) {
        let formData = new FormData();

        Object.keys(json).forEach(key => {
            let value = json[key];
            formData.append(key, value);
        });

        return formData;
    }


    /**
     * Creates a query-parameter string from a JSON object.
     * This method is used to send data to the server.
     *
     * @param {JSON} json JSON to convert into a query-paramter string
     *
     * @returns {string}
     */
    static createQueryString(json) {
        let parts = [];

        DebugUtility.log('Creating query string to send to the server.');

        Object.keys(json).forEach(key => {
            let value = json[key];

            // Escape post strings
            if (key === 'message' ||
                key === 'post_title' ||
                key === 'edit_title' ||
                key === 'thread_title' ||
                key === 'thread_subtitle') {
                value = GeneralUtility.encodeUtf8ToIso885915(value);
            }

            parts.push(key + '=' + value);
        });

        let queryString = parts.join('&');
        DebugUtility.log('Final query string: ' + queryString);
        return queryString;
    }


    /**
     * Does not fully work with the forum.
     * Converts an UTF8 string to ISO-8859-15.
     *
     * @param {string} string String to convert
     *
     * @returns {string}
     */
    static encodeUtf8ToIso885915(string) {
        // Accumulate messages outside of the RegExp loop and display later to prevent lags
        let debugMessages = [];

        string = string.replace(new RegExp(/([^\s0-9A-Za-zÀ-ž\.:,;*\-<>!"`´(){}[\]\^§$%&#+\-/\\=?|_]|[öäüÖÄÜ']|[\u0370-\u03FF\u0400-\u04FF]|\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/, 'giu'), function (char) {
            // The char-ranges in the previous RegExp seem to match regular characters like "k" in some cases
            // Very ugly quickfix: Check again, which basically fixes the OR-concatenated RegExp above
            let excludeRegExp = new RegExp(/[\s0-9A-Za-zÀ-ž\.:,;*\-<>!"`´(){}[\]\^§$%&#+\-/\\=?|_@]/, 'i');
            if (excludeRegExp.test(char)) {
                debugMessages.push('Character "' + char + '" is excluded from encoding.');
                return char;
            }

            // Convert char to codePoint
            let codePoint = (char.charCodeAt(0) - 0xD800) * 0x400 + char.charCodeAt(1) - 0xDC00 + 0x10000;
            debugMessages.push('Character "' + char + '" was converted to codePoint: ' + codePoint);

            // If no valid codePoint was found, use default JS codePoint
            if (isNaN(codePoint)) {
                codePoint = char.charCodeAt(0);
                debugMessages.push('Character "' + char + '" codePoint conversion returned NaN, use fallback codePoint: ' + codePoint);
            }

            // If still invalid, simply return the character itself
            if (isNaN(codePoint)) {
                debugMessages.push('Character "' + char + '" fallback codePoint was still NaN, use actual character as fallback: ' + char);
                return char;
            }

            return '&#' + codePoint + ';';
        });

        for (let i = 0; i < debugMessages.length; i++) {
            DebugUtility.log(debugMessages[i]);
        }

        // Manually escape plus-sign
        string = string.replace(new RegExp(/\+/, 'gi'), '&#43;');

        // \s => +
        string = string.replace(new RegExp(/ /, 'gi'), '+');

        DebugUtility.log('String before escape(): ' + string);
        string = escape(string);
        DebugUtility.log('String after escape(): ' + string);
        return string;
    }


    /**
     * Decodes HTML entities.
     *
     * @param {string} string String with HTML entities
     *
     * @returns {string}
     */
    static decodeHtmlEntities(string) {
        var element = document.createElement('textarea');
        element.innerHTML = string;
        return element.value;
    }


    /**
     * Gets an element from a JSON by a dot-separated path.
     *
     * @param {string|Array} path Dot-separated path to get or array with strings representing the path
     * @param {JSON} json JSON to get element from
     *
     * @returns {any|null} The element found at the path, if available
     */
    static getByPath(path, json) {
        if (!Array.isArray(path)) {
            path = path.split('.');
        }

        let key = path.shift();

        // All path-parts have been exhausted and the JSON is reduced to the final path, start returning the element up the recursion
        if (key === undefined) {
            return json;
        }

        if (json.hasOwnProperty(key)) {
            return GeneralUtility.getByPath(path, json[key])
        }

        // Element not found
        return null;
    }


    /**
     * Sets an element for a JSON at the given dot-separated path.
     *
     * @param {string|Array} path Dot-separated path to the element to set or array with strings representing the path
     * @param {any} value Value to set
     * @param {JSON} json JSON to set element in
     *
     * @returns {boolean} True when the value was set, otherwise false
     */
    static setByPath(path, value, json) {
        if (!Array.isArray(path)) {
            path = path.split('.');
        }

        let key = path.shift();

        // Parent of the target element is reached; set new value now by reference
        if (path.length === 0) {
            json[key] = value;
            return true;
        }

        if (json.hasOwnProperty(key)) {
            return GeneralUtility.setByPath(path, value, json[key])
        }

        // Element not found; nothing was set
        return false;
    }


    /**
     * Removes an element from a JSON by a dot-separated path.
     *
     * @param {string|Array} path Dot-separated path to get or array with strings representing the path
     * @param {JSON} json JSON to remove element from
     *
     * @returns {boolean} True when the element was deleted, otherwise false
     */
    static removeByPath(path, json) {
        if (!Array.isArray(path)) {
            path = path.split('.');
        }

        let key = path.shift();

        // Parent of the target element is reached; remove element now by reference
        if (path.length === 0 && json.hasOwnProperty(key)) {
            if (Array.isArray(json)) {
                var intKey = parseInt(key);
                if (!isNaN(intKey) && intKey > -1) {
                    json.splice(intKey, 1);
                    return true;
                }
            }

            delete json[key];
            return true;
        }

        if (json.hasOwnProperty(key)) {
            return GeneralUtility.removeByPath(path, json[key])
        }

        // Element not found; nothing was removed
        return false;
    }


    /**
     * Adds a CSS class to an element for a certain amount of time.
     *
     * @param {Element} element Element to add class to
     * @param {string} cssClass Name of the CSS class to add
     * @param {int} duration Time in milliseconds to keep the class
     *
     * @returns {void}
     */
    static addTimedClass(element, cssClass, duration) {
        element.classList.add(cssClass);
        setTimeout(() => {
            element.classList.remove(cssClass);
        }, duration);
    }


    /**
     * Creates a bootstrap alert.
     *
     * @param {string} type Type of the alert (danger, success, info, ...)
     * @param {string} content Content for the alert
     * @param {int} [duration] Time in milliseconds to show the alert
     *
     * @returns {Element}
     */
    static createAlert(type, content, duration) {
        let alert = document.createElement('div');
        alert.classList.add('alert', 'alert-' + type);
        alert.setAttribute('role', 'alert');
        alert.innerHTML = content;

        if (duration !== undefined && duration > 0) {
            setTimeout(() => {
                alert.remove();
            }, duration);
        }

        return alert;
    }


    /**
     * Creates a loading spinner.
     *
     * @param {string} [size='sm'] Size of the spinner sm, md, or lg
     * @param {boolean} [center=false] Whether to center the spinner in its container or not
     *
     * @returns {Element} Spinner or spinner container
     */
    static createSpinner(size, center) {
        if (size === undefined) {
            size = 'sm';
        }
        if (center === undefined) {
            center = false;
        }

        let spinner = document.createElement('img');
        spinner.classList.add('spinner', 'spinner-' + size);
        spinner.setAttribute('title', 'Loading...');

        // Apply image URL from the addons accessible resources
        let images = {
            'sm': 'mata-16.png',
            'md': 'mata-48.png',
            'lg': 'mata-128.png',
        };

        if (images.hasOwnProperty(size)) {
            spinner.setAttribute('src', browser.runtime.getURL('dist/resources/icons/' + images[size]));
        }

        if (center) {
            let container = document.createElement('div');
            container.classList.add('spinner-container');

            container.append(spinner);
            return container;
        }

        return spinner;
    }


    /**
     * Remove any spinners from a container.
     *
     * @param {Element} container Container to remove spinners from
     *
     * @returns {void}
     */
    static removeSpinners(container) {
        let spinners = container.querySelectorAll('.spinner');
        if (spinners) {
            spinners.forEach(spinner => {
                spinner.remove();
            });
        }
    }
}
