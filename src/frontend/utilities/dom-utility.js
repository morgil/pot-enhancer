/**
 * Utilities related to the DOM.
 *
 * @author FiveFuenf
 */
class DomUtility {
    constructor() {
    }


    /**
     * Gets the main HTML container.
     * Accessible in all routes.
     *
     * @returns {Node}
     */
    static get html() {
        if (DomUtility._html === null) {
            DomUtility._html = document.querySelector('html');
        }
        return DomUtility._html;
    }


    /**
     * Gets the main website body.
     * Accessible in all routes.
     *
     * @returns {Node}
     */
    static get body() {
        if (DomUtility._body === null) {
            DomUtility._body = document.querySelector('body');
        }
        return DomUtility._body;
    }


    /**
     * Gets the element responsible for scrolling the page.
     * Accessible in all routes.
     *
     * @returns {Node}
     */
    static get scrollingElement() {
        if (DomUtility._scrollingElement === null) {
            DomUtility._scrollingElement = (document.scrollingElement || document.body);
        }
        return DomUtility._scrollingElement;
    }


    /**
     * Gets the bookmarks container.
     * Accessible in the home route.
     *
     * @returns {Node}
     */
    static get bookmarksContainer() {
        if (DomUtility._bookmarksContainer === null) {
            DomUtility._bookmarksContainer = document.querySelector('#bookmarklist');
        }
        return DomUtility._bookmarksContainer;
    }


    /**
     * Gets the bookmark items.
     * Accessible in the home route.
     *
     * @returns {Node}
     */
    static get bookmarkItems() {
        if (DomUtility._bookmarkItems === null) {
            DomUtility._bookmarkItems = DomUtility.bookmarksContainer.querySelectorAll('table > tbody > tr');
        }
        return DomUtility._bookmarkItems;
    }


    /**
     * Gets the table containing the create post editor.
     * Accessible in the edit/create post/thread routes.
     *
     * @returns {Node}
     */
    static get postFormContainer() {
        if (DomUtility._postFormContainer === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[2];
            DomUtility._postFormContainer = temp.querySelector('table');
        }
        return DomUtility._postFormContainer;
    }


    /**
     * Gets the post/thread edit/create form element.
     * Accessible in the edit/create post/thread routes.
     *
     * @returns {Node}
     */
    static get postForm() {
        if (DomUtility._postForm === null) {
            let temp = DomUtility.postFormContainer;
            if (temp) {
                DomUtility._postForm = temp.querySelector('form');
            }
        }
        return DomUtility._postForm;
    }


    /**
     * Gets the container of the rendered preview on the preview page.
     * Accessible in the edit/create post routes.
     *
     * @returns {Node}
     */
    static get previewContainer() {
        if (DomUtility._previewContainer === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[2];
            temp = temp.querySelector('table');
            temp = temp.querySelectorAll('tbody > tr > td > table > tbody > tr')[2];
            DomUtility._previewContainer = temp.querySelector('td');
        }
        return DomUtility._previewContainer;
    }


    /**
     * Gets the container of the rendered preview on the thread preview page.
     * Accessible in the edit/create thread routes.
     *
     * @returns {Node}
     */
    static get threadPreviewContainer() {
        if (DomUtility._threadPreviewContainer === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[2];
            temp = temp.querySelector('table');
            temp = temp.querySelector('tbody > tr > td > table > tbody > tr');
            temp = temp.querySelectorAll('td > table > tbody > tr > td')[4];
            DomUtility._threadPreviewContainer = temp.querySelector('td');
        }
        return DomUtility._threadPreviewContainer;
    }


    /**
     * Gets the container holding all posts.
     * Accessible in the thread route.
     *
     * @returns {Node}
     */
    static get postsContainer() {
        if (DomUtility._postsContainer === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[2];
            temp = GeneralUtility.findChildren(temp, 'table')[1];
            DomUtility._postsContainer = temp.querySelector('tbody > tr > td > table > tbody');
        }
        return DomUtility._postsContainer;
    }


    /**
     * Gets all single post elements from the posts container.
     * Accessible in the thread route.
     *
     * @returns {NodeList}
     */
    static get posts() {
        if (DomUtility._posts === null) {
            let postsContainer = DomUtility.postsContainer;
            DomUtility._posts = postsContainer.querySelectorAll('tr[username]');
        }

        return DomUtility._posts;
    }


    /**
     * Gets the element holding the actual content of a post element.
     *
     * @returns {NodeList}
     */
    static get postContents() {
        if (DomUtility._postContents === null) {
            let postsContainer = DomUtility.postsContainer;
            DomUtility._postContents = postsContainer.querySelectorAll('span.posttext');
        }

        return DomUtility._postContents;
    }


    /**
     * Gets the table elements from the content of a post.
     *
     * @returns {NodeList}
     */
    static get postTables() {
        if (DomUtility._postTables === null) {
            let postsContainer = DomUtility.postsContainer;
            DomUtility._postTables = postsContainer.querySelectorAll('span.posttext > table[class]');
        }

        return DomUtility._postTables;
    }


    /**
     * Gets the code block elements from the content of a post.
     *
     * @returns {NodeList}
     */
    static get postCodeBlocks() {
        if (DomUtility._postCodeBlocks === null) {
            let postsContainer = DomUtility.postsContainer;
            DomUtility._postCodeBlocks = postsContainer.querySelectorAll('span.posttext > table:not([class])');
        }

        return DomUtility._postCodeBlocks;
    }


    /**
     * Gets the container holding all posts for a post preview.
     * Accessible in the create/edit post routes.
     *
     * @returns {Node}
     */
    static get previewPostsContainer() {
        if (DomUtility._previewPostsContainer === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[2];
            temp = GeneralUtility.findChildren(temp, 'table')[1];
            DomUtility._previewPostsContainer = temp.querySelector('tbody > tr > td > table > tbody');
        }
        return DomUtility._previewPostsContainer;
    }


    /**
     * Gets all single preview post elements from the preview posts container.
     * Accessible in the create/edit post routes.
     *
     * @returns {NodeList}
     */
    static get previewPosts() {
        if (DomUtility._previewPosts === null) {
            let previewPostsContainer = DomUtility.previewPostsContainer;
            DomUtility._previewPosts = GeneralUtility.findChildren(previewPostsContainer, 'tr');
        }

        return DomUtility._previewPosts;
    }


    /**
     * Gets the container holding the pagination for posts on the thread view.
     * Accessible in the thread route.
     *
     * @returns {Node}
     */
    static get postsPagination() {
        if (DomUtility._postsPagination === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[2];
            DomUtility._postsPagination = GeneralUtility.findChildren(temp, 'table')[2];
        }
        return DomUtility._postsPagination;
    }


    /**
     * Gets the container holding the thread tags on the bottom of the thread view.
     * Accessible in the thread route.
     *
     * @returns {Node}
     */
    static get threadTags() {
        if (DomUtility._threadTags === null) {
            DomUtility._threadTags = document.querySelector('form[action*="set_thread_groups"');
        }
        return DomUtility._threadTags;
    }


    /**
     * Gets the navbar on top of the board.
     * Accessible in all routes.
     *
     * @returns {Node}
     */
    static get navbar() {
        if (DomUtility._navbar === null) {
            let temp = GeneralUtility.findChildren(document.body, 'div[align="center"]')[1];
            DomUtility._navbar = temp.querySelector('.navbar');
        }
        return DomUtility._navbar;
    }
}


DomUtility._html = null;
DomUtility._body = null;
DomUtility._scrollingElement = null;
DomUtility._bookmarksContainer = null;
DomUtility._bookmarkItems = null;
DomUtility._postFormContainer = null;
DomUtility._postForm = null;
DomUtility._previewContainer = null;
DomUtility._threadPreviewContainer = null;
DomUtility._postsContainer = null;
DomUtility._previewPostsContainer = null;
DomUtility._posts = null;
DomUtility._postContents = null;
DomUtility._postTables = null;
DomUtility._postCodeBlocks = null;
DomUtility._previewPosts = null;
DomUtility._postsPagination = null;
DomUtility._threadTags = null;
DomUtility._navbar = null;
