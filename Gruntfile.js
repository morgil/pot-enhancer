'use strict';

module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    const sass = require('node-sass');

    grunt.initConfig({
        'sass': {
            'options': {
                'implementation': sass,
                'sourceMap': true
            },
            'frontend': {
                'files': {
                    './dist/frontend/css/frontend.css': './src/frontend/scss/frontend.scss'
                }
            },
            'backend': {
                'files': {
                    './dist/backend/css/backend.css': './src/backend/scss/backend.scss'
                }
            }
        },
        'cssmin': {
            'options': {
                'sourceMap': true,
                'mergeIntoShorthands': false,
                'relativeTo': './dist/',
                'rebase': true
            },
            'frontend': {
                'files': {
                    './dist/frontend/css/frontend.min.css': './dist/frontend/css/frontend.css'
                }
            },
            'backend': {
                'files': {
                    './dist/backend/css/backend.min.css': './dist/backend/css/backend.css'
                }
            }
        },
        'uglify': {
            'options': {
                'sourceMap': true
            },
            'frontend': {
                'files': {
                    './dist/frontend/js/frontend.min.js': [
                        './src/frontend/utilities/*.js',
                        './src/backend/settings/settings.js',
                        './src/frontend/routes/route.js',
                        './src/frontend/routes/*.js',
                        './src/frontend/components/*.js',
                        './src/frontend/components/editor/abstract-editor.js',
                        './src/frontend/components/editor/*',
                        './src/frontend/components/posts/*.js',
                        './src/frontend/components/block-users/*.js',
                        './src/frontend/components/bookmarks/*.js',
                        './src/frontend/components/navbar/*.js',
                        './src/backend/components/save-posts/save-post.js',
                        './src/frontend/router.js',
                        './src/frontend/frontend.js'
                    ]
                }
            },
            'backend': {
                'files': {
                    './dist/backend/js/backend.min.js': [
                        './src/frontend/utilities/*.js',
                        './src/frontend/components/*.js',
                        './src/backend/settings/*.js',
                        './src/backend/components/**/*.js',
                        './src/backend/backend.js'
                    ]
                }
            }
        },
        'copy': {
            'bootstrap': {
                'expand': true,
                'cwd': './node_modules/bootstrap/dist/js/',
                'src': '**',
                'dest': './dist/libraries/bootstrap/'
            }
        },
        'watch': {
            'js': {
                'files': [
                    './src/frontend/**/*.js',
                    './src/backend/**/*.js'
                ],
                'tasks': ['uglify']
            },
            'scss': {
                'files': [
                    './src/frontend/scss/**/*.scss',
                    './src/backend/scss/**/*.scss',
                    './src/scss/**/*.scss',
                ],
                'tasks': ['sass', 'cssmin']
            }
        }
    });

    grunt.registerTask('js', ['uglify']);
    grunt.registerTask('scss', ['sass', 'cssmin']);
    grunt.registerTask('default', ['sass', 'cssmin', 'uglify', 'copy']);
};
