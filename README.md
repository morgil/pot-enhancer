# pOT Enhancer

Enhances the experience on forum.mods.de. Provides extensive configurability through extension options.

# Features

- Enhanced post editors.
- Quick-Post editor.
- Remember post messages so that they can be retrieved if the page is lost.
- Create custom emotes.
- Block users.
- Save/bookmark posts.
- Open bookmarks in a new tab.
- Pin bookmarks to the top of the list.
- Reorder bookmarks.
- Inject custom CSS. The following classes will be available in the `<body>`-tag to target specific routes:
    - `home`: When on the startpage.
    - `board`: When viewing a board/thread listing.
    - `thread`: When viewing posts within a thread.
    - `create-thread`: When viewing the create a new thread-form.
    - `create-post`: When viewing the create a new post-form.
    - `edit-post`: When viewing the edit a post-form.
- Quote last post, if the new post would be on a new page.
- Save and restore settings from extension options.
- Styling improvements to the site in general.

# Development, build and source code

- Run `npm run dev` to start the web-ext development browser.
- Run `npm run build`to build the final Firefox addon.
- Run `npm run grunt-build` to compile the SCSS files to CSS and uglify the vanilla JS files into a single file.
- Run `npm run grunt-watch` to watch for changes in the SCSS and JS files and run builds automatically.

`npm run dev` can be extended with custom parameters, e.g. for providing a specific profile for Firefox-executable to use:

````powershell
npm run dev -- --verbose --keep-profile-changes --firefox-profile 'C:\Users\user\AppData\Roaming\Mozilla\Firefox\Profiles\web-ext-dev' --firefox 'C:\Program Files\Firefox Developer Edition\firefox.exe'
````

All SCSS, JS or other source files of this project can be found in the `src` folder. Run `npm install` to load all packages required by NPM/Node.
The built files can be found in the `dist` folder.

This addon uses [Bootstrap v5](https://getbootstrap.com/).
